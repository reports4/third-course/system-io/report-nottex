#ifndef LLB_1_MODULE
#define LLB_1_MODULE

#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/uaccess.h>
#include <linux/list.h>
#include <linux/slab.h>

#ifndef CLASS_NAME
	#define CLASS_NAME "llb"
#endif

#ifndef SUCCESS
	#define SUCCESS 0
#endif

#ifndef ERRORS_CODE
	#define ERRORS_CODE
	#define NON_ERROR 0
	#define ZERO_DIVISION 1
#endif

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Artem Kiselev, Victor Shcherbakov");
MODULE_DESCRIPTION("A Linux module for SIO laboratory work #1.");
MODULE_VERSION("0.1");

static struct proc_dir_entry* entry;
int arifmeticError = NON_ERROR;

static int majorNumber = 0;
static struct device* c_dev;
static struct class* cl;

static int device_open = 0;

struct list_res {
	struct list_head list;
	long result;
	int error;
};

static struct list_head head_res;

static int ch_open(struct inode*, struct file*);
static int ch_close(struct inode*, struct file*);
static ssize_t ch_read(struct file*, char __user*, size_t, loff_t*);
static ssize_t ch_write(struct file*, const char __user*, size_t, loff_t*);
static ssize_t proc_read(struct file*, char __user*, size_t, loff_t*);

static int list_length(struct list_head *head_ptr) {
	int len = 0;
	struct list_head *ptr;
	list_for_each(ptr, head_ptr) {
		len++;
	}
	return len;
}

// Variant 1
#ifdef LAB_VARIANT1
#define DEVICE_NAME "var1"

static long process(const char __user* buffer, size_t len, loff_t* offset) {
	long result = 0;
	int bytes_left = 0;

	size_t i;
	size_t start = *offset;
	size_t stop = start + len;
	
	for (i = start; i < stop; i++) {
		unsigned char ch;
		get_user(ch, buffer + i);
	
		if (bytes_left > 0) {
			bytes_left--;
			continue;
		}
		
		if( (// ASCII use bytes[0] <= 0x7F to allow ASCII control characters
                (0x20 <= ch && ch <= 0x7E)
            )
        ) {
			result++;
		} else if ((// non-overlong 2-byte
                (0xC2 <= ch && ch <= 0xDF)
            )) {
			result++;
			bytes_left = 1;
		} else if ((// excluding overlongs
                ch == 0xE0
            ) ||
            (// straight 3-byte
                ((0xE1 <= ch && ch <= 0xEC) ||
                    ch == 0xEE ||
                    ch == 0xEF)
            ) ||
            (// excluding surrogates
                ch == 0xED
            )) {
			result++;
			bytes_left = 2;
		} else if ((// planes 1-3
                ch == 0xF0
            ) ||
            (// planes 4-15
                (0xF1 <= ch && ch <= 0xF3)
            ) ||
            (// plane 16
                ch == 0xF4 
            )) {
			result++
			bytes_left = 3;
		}
	}
	return result;
}

#endif

// Variant 2
#ifdef LAB_VARIANT2
#define DEVICE_NAME "var2"
static long process(const char __user* buffer, size_t len, loff_t* offset) {
	long result;

	int op1 = 0;
	int op2 = 0;
	int first = 1;
	char op = ' ';

	size_t i;
	size_t start = *offset;
	size_t stop = start + len;

	for (i = start; i < stop; i++) {
		char getChar;
		get_user(getChar, buffer + i);

		if (getChar >= '0' && getChar <= '9') {
			if (first) op1 = op1 * 10 + (getChar - '0');
			else       op2 = op2 * 10 + (getChar - '0');
		}

		if(getChar == '+' || getChar == '-' || getChar == '/' || getChar == '*') {
			if (first) {
				first = 0;
				op = getChar;
			}else {
				len = i;
				break;
			}
		}
	}

	if (op == '+') {
		result = op1 + op2;
	}
	else if (op == '-') {
		result = op1 - op2;
	}
	else if (op == '*') {
		result = op1 * op2;
	}
	else if (op == '/') {
		if (op2 == 0) {
			result = 0;
			arifmeticError = ZERO_DIVISION;
			return len;
		}
		result = op1 / op2;
	}

	arifmeticError = NON_ERROR;
	
	return result;
}
#endif

// Variant 3
#ifdef LAB_VARIANT3
#define DEVICE_NAME "var3"
static long process(const char __user* buffer, size_t len, loff_t* offset) {
	long result = 0;

	int num = 0;

	size_t i;
	size_t start = *offset;
	size_t stop = start + len;

	for (i = start; i < stop; i++) {
		char getChar;
		get_user(getChar, buffer + i);

		if (getChar >= '0' && getChar <= '9') {
			num = num * 10 + (getChar - '0');
		} else {
			result += num;
			num = 0;
		}
	}
	
	return result;
}
#endif

// Variant 4
#ifdef LAB_VARIANT4
#define DEVICE_NAME "var4"
static long process(const char __user* buffer, size_t len, loff_t* offset) {
	long result = 0;
	
	size_t i;
	size_t start = *offset;
	size_t stop = start + len;

	for (i = start; i < stop; i++) {
		char ch;
		get_user(ch, buffer + i);
		
		if (ch == ' ')
			result++;
	}
	return result;
}
#endif

// Variant 5
#ifdef LAB_VARIANT5
#define DEVICE_NAME "var5"
static long process(const char __user* buffer, size_t len, loff_t* offset) {
	long result = 0;
	
	size_t i;
	size_t start = *offset;
	size_t stop = start + len;

	for (i = start; i < stop; i++) {
		char ch;
		get_user(ch, buffer + i);
		
		if (ch >= 'A' && ch <= 'Z')
			result++;
		if (ch >= 'a' && ch <= 'z')
			result++;
	}
	return result;
}
#endif

static struct file_operations ch_ops = {
	.owner = THIS_MODULE,
	.open = ch_open,
	.release = ch_close,
	.read = ch_read,
	.write = ch_write 
};

static struct file_operations proc_fops = {
	.owner = THIS_MODULE,
	.read = proc_read
};

static int ch_open(struct inode* inode, struct file* file) {
	printk(KERN_NOTICE "%s: open()\n", THIS_MODULE->name);

	if (device_open)
		return -EBUSY;

	device_open++;

	try_module_get(THIS_MODULE);
	return SUCCESS;
}

static int ch_close(struct inode* i, struct file* f){
	printk(KERN_INFO "%s: close()\n", THIS_MODULE->name);
	device_open--;

	module_put(THIS_MODULE);
	return SUCCESS;
}

static ssize_t ch_read(struct file* f, char __user* buf, size_t len, loff_t* off){
	printk(KERN_INFO "%s: read()\n", THIS_MODULE->name);
	return len;
}

static ssize_t ch_write(struct file* filep, const char __user* buffer, size_t len, loff_t* offset){
	printk(KERN_INFO "%s: write()\n", THIS_MODULE->name);

	struct list_res *r = kmalloc(sizeof(struct list_res *), GFP_KERNEL);
	r->result = process(buffer, len, offset);
	r->error = arifmeticError;
	list_add(&r->list, &head_res);

	return *offset + len;
}

#define LONG_STR_LEN 25

static ssize_t proc_read(struct file* file, char __user *ubuf, size_t count, loff_t* ppos) {
	char *buf = kzalloc(sizeof(char) * LONG_STR_LEN * list_length(&head_res), GFP_KERNEL);
	
	struct list_head *ptr;
	struct list_res *entry;
	size_t i = 0;

	list_for_each(ptr, &head_res) {
		entry = list_entry(ptr, struct list_res, list);
		if(entry->error == NON_ERROR) {
			snprintf(buf+(i*LONG_STR_LEN), LONG_STR_LEN, "%ld\n", entry->result);
			printk(KERN_INFO "%s Result %ld: %ld\n", THIS_MODULE->name, i, entry->result);
		}else if(entry->error == ZERO_DIVISION) {
			snprintf(buf+(i*LONG_STR_LEN), LONG_STR_LEN, "%s\n", "ERR: ZeroDivision");
			printk(KERN_INFO "%s Result %ld: %s\n", THIS_MODULE->name, i, "ERR: ZeroDivision");
		}
		i++;
	}

	size_t len = LONG_STR_LEN * list_length(&head_res);

	if (*ppos > 0 || count < len){
		return 0;
	}
	if (copy_to_user(ubuf, buf, len) != 0){
		return -EFAULT;
	}

	*ppos = len;

	kfree(buf);

	return len;
}

static int __init initModule(void)
{
	printk(KERN_INFO "Load LabModule1");

	majorNumber = register_chrdev(majorNumber, DEVICE_NAME, &ch_ops);
	if (majorNumber < 0)
	{
		printk(KERN_ALERT "%s: failed to register a major number\n", THIS_MODULE->name);
		return majorNumber;
	}
	printk(KERN_INFO "%s: registered correctly with major number %d\n", THIS_MODULE->name, majorNumber);

	cl = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(cl)) { 
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "%s: Failed to register device class\n", THIS_MODULE->name);
		return PTR_ERR(cl);
	}
	printk(KERN_INFO "%s: device class registered correctly\n", THIS_MODULE->name);

	c_dev = device_create(cl, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
	if (IS_ERR(c_dev)) {	   
		class_destroy(cl);
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "%s: Failed to create the device\n", THIS_MODULE->name);
		return PTR_ERR(c_dev);
	}
	printk(KERN_INFO "%s: device class created correctly\n", THIS_MODULE->name);
	printk(KERN_INFO "%s: /dev/%s is created\n", THIS_MODULE->name, DEVICE_NAME);

	entry = proc_create(DEVICE_NAME, 0444, NULL, &proc_fops);
	printk(KERN_INFO "%s: /proc/%s is created\n", THIS_MODULE->name, DEVICE_NAME);
	
	INIT_LIST_HEAD(&head_res);

	return SUCCESS;
}

static void __exit exitModule(void)
{
	printk(KERN_INFO "Unload LabModule1");
	
	device_destroy(cl, MKDEV(majorNumber, 0));
	class_unregister(cl);
	class_destroy(cl);
	unregister_chrdev(majorNumber, DEVICE_NAME);
	printk(KERN_INFO "%s: /dev/%s is destroy\n", THIS_MODULE->name, DEVICE_NAME);

	proc_remove(entry);
	printk(KERN_INFO "%s: /proc/%s is destroy\n", THIS_MODULE->name, DEVICE_NAME);

	struct list_res *entry, *next;
	
	list_for_each_entry_safe(entry, next, &head_res, list) {
		list_del(&entry->list);
		kfree(entry);
	}
}

module_init(initModule);
module_exit(exitModule);

#endif
